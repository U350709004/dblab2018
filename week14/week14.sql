
CREATE TABLE `employees_backup`
(
	`id` INT auto_increment,
	`EmployeeID` int(11) NOT NULL,
    `LastName` varchar(45) NOT NULL,
    `FirstName` varchar(45) NOT NULL,
	`BirthDate` varchar(45) NOT NULL,
    `changedat` DATETIME DEFAULT NULL,
	`action` VARCHAR(50) DEFAULT NULL,
    PRIMARY KEY (`id`)

);

select * from employees_backup;
update employees set LastName = 'olmez' where EmployeeID = 6;
insert into employees (LastName, FirstName, BirthDate, Photo, Notes)
	values ("Turk", "Erdem", "1990-01-01", "EmpID11.pic", "Intern");
delete from employees where EmployeeID = 11;